import imutils
import cv2
from datetime import datetime

vcap = cv2.VideoCapture("http://10.100.100.3:9090/?action=stream")

def rotate90(frame):
    (h, w) = frame.shape[:2]
    center = (w / 2, h / 2)
    scale = 1.4     #Scale the image so that there are no borders
    M = cv2.getRotationMatrix2D(center, 90, scale)
    rotated90 = cv2.warpAffine(frame, M, (h, w))
    return rotated90 

while(1):
    ret, frame = vcap.read()
    frame = rotate90(frame)
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

vcap.release()
cv2.destroyAllWindows()
