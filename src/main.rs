extern crate cursive;
use cursive::theme::{Color, PaletteColor, Theme};
use cursive::traits::*;
use cursive::vec::Vec2;
use cursive::{Cursive, Printer};
use std::collections::VecDeque;
use std::sync::mpsc;
use std::thread;
use std::time::{Instant, Duration};

fn main() {
    let mut siv = Cursive::default();
    let theme = custom_theme_from_cursive(&siv);
    siv.set_theme(theme);
    let cb_sink = siv.cb_sink().clone();
    siv.add_global_callback('q', |s| s.quit());
    let (tx, rx) = mpsc::channel();
    // Generate timestamps in a separate thread.
    thread::spawn(move || {
        generate_timestamp(&tx, cb_sink);
    });
    let timesample_buffer = 50;             
    siv.add_layer(BufferView::new(timesample_buffer, rx).fixed_size((40, 30)));
    siv.run();
}

fn generate_timestamp(tx: &mpsc::Sender<String>, cb_sink: cursive::CbSink) {
    let now = Instant::now();
    loop {
        let timestamp = now.elapsed().as_millis();
        let line = format!("{:?}", timestamp);
        if tx.send(line).is_err() {
            return;
        }
        cb_sink.send(Box::new(Cursive::noop)).unwrap();
        thread::sleep(Duration::from_millis(10));
    }
}

struct BufferView {
    buffer: VecDeque<String>,
    rx: mpsc::Receiver<String>,
}

impl BufferView {
    fn new(size: usize, rx: mpsc::Receiver<String>) -> Self {
        let mut buffer = VecDeque::new();
        buffer.resize(size, String::new());
        BufferView {
            rx: rx,
            buffer: buffer,
        }
    }
    fn update(&mut self) {
        while let Ok(line) = self.rx.try_recv() {
            self.buffer.push_front(line);
            self.buffer.pop_back();
        }
    }
}

impl View for BufferView {
    fn layout(&mut self, _: Vec2) {
        self.update();
    }
    fn draw(&self, printer: &Printer) {
        // Print the end of the buffer
        for (i, line) in
            self.buffer.iter().rev().take(printer.size.y).enumerate()
        {
            printer.print((0, printer.size.y - 1 - i), line);
        }
    }
}

fn custom_theme_from_cursive(siv: &Cursive) ->
    Theme {
        let mut theme = siv.current_theme().clone();
        theme.palette[PaletteColor::Background] = Color::TerminalDefault;
        theme.palette[PaletteColor::View] = Color::TerminalDefault;
        theme.palette[PaletteColor::Primary] = Color::TerminalDefault;
        theme
    }

